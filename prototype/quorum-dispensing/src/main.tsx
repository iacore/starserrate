import { render } from "solid-js/web";
import { For, createSignal } from "solid-js";
import "./main.css";

function Counter() {
  const [get_payload, set_payload] = createSignal("");

  const cells: string[] = [];
  for (let i = 0; i < 50; ++i) cells[i] = "";
  let [get_cells, set_cells] = createSignal(cells, { equals: () => false });
  const release = () => {
    for (let c of get_payload()) {
      cells[Math.floor(Math.random() * cells.length)] = c;
    }
    set_cells(cells);
    set_payload("")
  };

  return (
    <div>
      <div>
        <input
          placeholder="Chemicals"
          value={get_payload()}
          onInput={(e) => set_payload(e.target.value)}
        />
        <button type="button" onClick={release}>
          Release
        </button>
      </div>
      <div>
        <div class="cells">
          <For each={get_cells()}>{(s) => <div>{s}</div>}</For>
        </div>
      </div>
      <div>Quorum</div>
    </div>
  );
}

render(() => <Counter />, document.getElementById("app")!);
