import { Show, createSignal } from 'solid-js'
import solidLogo from './assets/solid.svg'
import viteLogo from '/vite.svg'
import './App.css'

const on = () => <div class="flex flex-col w-1 gap-0.5">
  <div class="h-1 bg-black" />
  <div class="bg-black grow" />
</div>
const off = () => <div class="flex flex-col w-1 gap-0.5">
  <div class="h-1 bg-black" />
</div>

function App() {
  const [showSidebarLeft, set_showSidebarLeft] = createSignal(false)

  return (
    <div class="h-screen overflow-hidden flex flex-col">
      <header class="flex gap-1 justify-between">
        <h1 class="flex p-1">awawawawa</h1>
        <div class="flex gap-2 p-1">
          <div class="flex gap-0.5">
            {on()}
            {off()}
            {on()}
            {on()}
            {off()}
            {off()}
          </div>
          Nostr
        </div>
      </header>
      <main class="grow flex justify-between">
        <div class="flex">
          <button onClick={() => set_showSidebarLeft(!showSidebarLeft())}>{">"}</button>
          <Show when={showSidebarLeft()}>
            <div class="p-1">
              <div class="cells">
                <For each={Array.from({ length: 60 })}>{() => <div></div>}</For>
              </div>
              <h2 class="text-center">Quorum</h2>
            </div>
          </Show>
        </div>

        <div class="flex flex-col justify-end">
          <div class="p-4">
            <h2 class="text-center">raster output</h2>
            <div class="leading-none" style="letter-spacing:-2px;">
              {/* mock */}
              ⣿⣿⠀⣿⣿⣿⣿⣿⣿⠀⠀⠀<br />
              ⣿⣿⠀⣿⣿⣿⣿⣿⣿⠀⠀⠀<br />
              ⣿⣿⠀⣿⣿⣿⠀⣿⣿⠀⠀⠀<br />
              ⣿⣿⠀⠀⠀⠀⠀⣿⣿⠀⠀⠀<br />
              ⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿<br />
              ⣿⣿⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀<br />
              ⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿<br />
            </div>
          </div>
        </div>

        <div class="flex flex-col justify-end">
          <div class="p-4">
            <h2 class="text-center">vector output</h2>
            {/* todo */}
          </div>
        </div>
      </main>
    </div>
  )
}

export default App
