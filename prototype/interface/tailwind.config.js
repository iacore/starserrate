/** @type {import('tailwindcss').Config} */
export default {
  content: ["src/**"],
  theme: {
    extend: {
      colors: {
        pastel: {
          0: "#e2cfc4ff", // champagne-pink
          1: "#faedcbff", // dutch-white
          2: "#dbcdf0ff", // thistle
          3: "#c6def1ff", // mint-green
          4: "#f2c6deff", // fairy-tale
          5: "#f7d9c4ff", // champagne-pink-2
        }
      }
    },
  },
  plugins: [],
}

