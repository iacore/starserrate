const std = @import("std");
const positron = @import("positron");
const mustache = @import("mustache");

var global_gpa = std.heap.GeneralPurposeAllocator(.{}){};
const global_allocator = global_gpa.allocator();

const host = "localhost";
const port = 5000;

const State = struct {
    counter: i32,
    mutex: std.Thread.Mutex = .{},
};
var test_state: State = .{ .counter = 42 };

pub fn main() !void {
    const tid = try std.Thread.spawn(.{ .allocator = global_allocator }, run_server_wrapper, .{});
    tid.detach(); // ignore until program exits
    const url = try std.fmt.allocPrintZ(global_allocator, "http://{s}:{}/", .{ host, port });
    defer global_allocator.free(url);
    try run_webview(url);
}

pub fn run_webview(url: [:0]const u8) !void {
    var webview = try positron.View.create(true, null);
    defer webview.destroy();
    webview.navigate(url);
    webview.run();
}

pub fn run_server_wrapper() void {
    run_server() catch |e| {
        std.log.err("{}", .{e});
        if (@errorReturnTrace()) |st| std.debug.dumpStackTrace(st.*);
        std.os.exit(1);
    };
}
pub fn run_server() !void {
    var server = std.http.Server.init(global_allocator, .{});
    defer server.deinit();
    const list = try std.net.getAddressList(global_allocator, host, port);
    defer list.deinit();
    try server.listen(list.addrs[0]);
    std.log.info("Listening on http://{s}:{}/", .{ host, port });
    while (true) {
        var response = try server.accept(.{ .allocator = global_allocator });
        const tid = try std.Thread.spawn(.{ .allocator = global_allocator }, handle_response, .{response});
        tid.detach();
    }
}
fn handle_response(_response: std.http.Server.Response) void {
    var response = _response;
    defer response.deinit();
    while (true) {
        process_response(&response, &test_state) catch break;
        if (response.reset() == .closing) break;
    }
}
fn process_response(response: *std.http.Server.Response, state: *State) !void {
    try response.wait();
    if (response.request.method == .POST) {
        state.mutex.lock();
        state.counter += 1;
        state.mutex.unlock();
    }
    var buffer: [1024 * 16]u8 = undefined;
    const template = try std.fs.cwd().readFile("template/index.html", &buffer);
    const body = try mustache.allocRenderText(global_allocator, template, state);
    defer global_allocator.free(body);
    response.transfer_encoding = .{ .content_length = @intCast(body.len) };
    try response.do();
    try response.writeAll(body);
    try response.finish();
}
